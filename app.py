from flask import Flask, render_template, url_for, request
from flask_mail import Mail, Message


app = Flask(__name__)
app.config.from_envvar('YOURAPPLICATION_SETTINGS')
mail = Mail(app)

def render_index():
    styles_path = url_for('static', filename='styles/style.css')
    js_path = url_for('static', filename='scripts/script.js')
    img_path = url_for('static', filename='images/')
    settings = {
        'styles_path': styles_path,
        'js_path': js_path,
        'img_path': img_path
    }
    msg = Message("Hello",
                  sender="test@bas80.tw1.ru",
                  recipients=["test@bas80.tw1.ru"])
    mail.send(msg)
    return render_template('index.html', settings=settings)


@app.route('/')
def index():
    return render_index()


@app.route('/send', methods=['POST'])
def send_message():
    print(request.form)
    return render_index()


app.run(port=5000)
